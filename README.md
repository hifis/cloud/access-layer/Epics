# Epics

The purpose of this project is to define Epics within issues of the project.

The scope of the Epics is the Helmholtz Cloud Access Layer. This may encompass Epics which will be implemented there, but also Epics of items which are required by it.